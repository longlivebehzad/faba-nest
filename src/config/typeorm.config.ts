import { TypeOrmModuleOptions } from '@nestjs/typeorm';
const config = require('config');

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: config.get('database.type'),
  host: config.get('database.host'),
  port: config.get('database.port'),
  database: config.get('database.name'),
  username: config.get('database.username'),
  password: config.get('database.password'),
  entities: [__dirname + './../**/*.entity.js'],
  synchronize: true,
  useUnifiedTopology: true,
};
