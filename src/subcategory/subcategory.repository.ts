import { Category } from './../category/category.entity';
import { createSubCategoryDto } from './dto/create-subcategory.dto';
import { Subcategory } from './subcategory.entity';
import { User } from 'src/auth/user.entity';

import { Repository, EntityRepository } from 'typeorm';
import {
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common';

@EntityRepository(Subcategory)
export class SubCategoryRepository extends Repository<Subcategory> {
  async createSubCategory(category: Category, name: string): Promise<any> {
    try {
      const subCategory = new Subcategory();
      subCategory.name = name;
      subCategory.category = category;

      await subCategory.save();
      return subCategory;
    } catch (error) {
      throw error;
    }
  }

  async getSubCategories(categoryId: number) {
    const query = this.createQueryBuilder('subcategory');

    try {
      return query
        .where('subcategory.category = :categoryId')
        .setParameters({ categoryId })
        .getMany();
    } catch (error) {
      throw error;
    }
  }
}
