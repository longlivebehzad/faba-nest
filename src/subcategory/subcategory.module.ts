import { CategoryRepository } from './../category/category.repository';
import { SubCategoryRepository } from './subcategory.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './../auth/auth.module';
import { PassportModule } from '@nestjs/passport';
import { Module } from '@nestjs/common';
import { SubcategoryController } from './subcategory.controller';
import { SubcategoryService } from './subcategory.service';

@Module({
  imports: [
    PassportModule,
    AuthModule,
    TypeOrmModule.forFeature([SubCategoryRepository, CategoryRepository]),
  ],
  controllers: [SubcategoryController],
  providers: [SubcategoryService],
})
export class SubcategoryModule {}
