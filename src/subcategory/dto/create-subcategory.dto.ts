import { IsString, IsNumber } from 'class-validator';

export class createSubCategoryDto {
  @IsString()
  name: string;

  @IsNumber()
  category: number;
}
