import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './../category/category.entity';
import { createSubCategoryDto } from './dto/create-subcategory.dto';
import { User } from 'src/auth/user.entity';
import { CategoryRepository } from './../category/category.repository';
import { SubCategoryRepository } from './subcategory.repository';
import { Injectable, InternalServerErrorException } from '@nestjs/common';

@Injectable()
export class SubcategoryService {
  constructor(
    @InjectRepository(SubCategoryRepository)
    private subCategoryRepository: SubCategoryRepository,
    @InjectRepository(CategoryRepository)
    private categoryRepository: CategoryRepository,
  ) {}

  async createSubCategory(createSubCategoryDto: createSubCategoryDto) {
    const { category: categoryId, name } = createSubCategoryDto;
    try {
      const category: Category = await this.categoryRepository.findOne({
        id: categoryId,
      });
      return this.subCategoryRepository.createSubCategory(category, name);
    } catch (error) {
      throw new InternalServerErrorException('Server Error!');
    }
  }

  async getSubCategories(categoryId: number) {
    return this.subCategoryRepository.getSubCategories(categoryId);
  }
}
