import { createSubCategoryDto } from './dto/create-subcategory.dto';
import { SubcategoryService } from './subcategory.service';
import {
  Controller,
  Get,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Body,
  Post,
  Param,
} from '@nestjs/common';

import { AuthGuard } from '@nestjs/passport';

@Controller('subcategory')
export class SubcategoryController {
  constructor(private subcategoryService: SubcategoryService) {}

  @UsePipes(ValidationPipe)
  @UseGuards(AuthGuard())
  @Post('')
  async createSubCategory(
    @Body() createSubCategoryDto: createSubCategoryDto,
  ): Promise<any> {
    return this.subcategoryService.createSubCategory(createSubCategoryDto);
  }

  @UseGuards(AuthGuard())
  @Get('/:categoryId')
  async getSubCategories(
    @Param('categoryId') categoryId: number,
  ): Promise<any> {
    return this.subcategoryService.getSubCategories(categoryId);
  }
}
