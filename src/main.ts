import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
const config = require('config');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  const whitelist = ['http://localhost:3000'];
  app.enableCors({
    origin: function(origin, callback) {
      return callback(null, true);
      if (whitelist.indexOf(origin) !== -1) {
        console.log('allowed cors for:', origin);
        callback(null, true);
      } else {
        console.log('blocked cors for:', origin);
        callback(new Error('Not allowed by CORS'));
      }
    },
    allowedHeaders: '*',
    methods: 'GET,PUT,POST,DELETE,UPDATE,OPTIONS',
    credentials: true,
  });

  console.log(config.get('server.port'));

  await app.listen(config.get('server.port'));
}
bootstrap();
