import { VerifyHashDto } from './dto/verify-hash.dto';
import { User } from './../auth/user.entity';
import { AuthGuard } from '@nestjs/passport';
import { Hash } from './hash.entity';
import { CreateHashDto } from './dto/create-hash.dto';
import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  ValidationPipe,
  UsePipes,
  UseGuards,
  Param,
} from '@nestjs/common';
import { HashesService } from './hashes.service';
import { GetUser } from 'src/auth/get-user.decorator';

@Controller('hash')
export class HashesController {
  constructor(private hashesService: HashesService) {}

  @Post('')
  @UseGuards(AuthGuard())
  async createHash(
    @GetUser() user: User,
    @Body() createHashDto: CreateHashDto,
  ) {
    return await this.hashesService.createHash(user, createHashDto);
  }

  @Get('')
  @UseGuards(AuthGuard())
  async getUserCategory(@GetUser() user: User): Promise<Hash[]> {
    return await this.hashesService.getUserCategory(user);
  }
}
