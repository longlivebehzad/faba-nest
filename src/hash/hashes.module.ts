import { SubCategoryRepository } from './../subcategory/subcategory.repository';
import { CategoryRepository } from './../category/category.repository';
import { PassportModule } from '@nestjs/passport';
import { AuthModule } from './../auth/auth.module';
import { HashesService } from './hashes.service';
import { HashesController } from './hashes.controller';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HashRepository } from './hash.repository';

@Module({
  imports: [
    AuthModule,
    TypeOrmModule.forFeature([
      HashRepository,
      CategoryRepository,
      SubCategoryRepository,
    ]),
  ],
  controllers: [HashesController],
  providers: [HashesService],
  exports: [HashesService],
})
export class HashesModule {}
