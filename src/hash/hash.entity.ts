import { Subcategory } from './../subcategory/subcategory.entity';
import { Category } from './../category/category.entity';
import { User } from 'src/auth/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToOne,
  JoinColumn,
  BaseEntity,
  OneToMany,
} from 'typeorm';

@Entity()
export class Hash extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  hash: string;

  @Column()
  transaction: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  fatherName: string;

  @Column()
  nationalId: string;

  @Column()
  certId: string;

  @ManyToOne(
    type => User,
    user => user.hashes,
  )
  user: User;

  @ManyToOne(type => Category)
  category: Category;

  @ManyToOne(type => Subcategory)
  subcategory: Subcategory;
}
