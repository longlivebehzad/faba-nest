import { Subcategory } from './../subcategory/subcategory.entity';
import { Category } from './../category/category.entity';
import { VerifyHashDto } from './dto/verify-hash.dto';
import { User } from './../auth/user.entity';
import { CreateHashDto } from './dto/create-hash.dto';
import { Hash } from './hash.entity';
import { Repository, EntityRepository } from 'typeorm';
import {
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common';

@EntityRepository(Hash)
export class HashRepository extends Repository<Hash> {
  private sabtShod;
  constructor() {
    super();
  }

  async createHash(
    user: User,
    createHashDto: CreateHashDto,
    category: Category,
    subcategory: Subcategory,
  ): Promise<Hash> {
    const {
      hash,
      transaction,
      firstName,
      lastName,
      fatherName,
      nationalId,
      certId,
    } = createHashDto;
    try {
      const newHash = new Hash();
      newHash.user = user;
      newHash.hash = hash;
      newHash.firstName = firstName;
      newHash.lastName = lastName;
      newHash.fatherName = fatherName;
      newHash.nationalId = nationalId;
      newHash.certId = certId;
      newHash.transaction = transaction;
      newHash.category = category;
      newHash.subcategory = subcategory;

      await newHash.save();

      return newHash;
    } catch (error) {
      console.log(error);

      throw new InternalServerErrorException();
    }
  }

  async getUserCategory(user: User): Promise<Hash[]> {
    const query = this.createQueryBuilder('hash');
    const hashes: Hash[] = await query
      .where('hash.user = :user')
      .setParameters({ user: user.id })
      .leftJoinAndSelect('hash.category', 'category')
      .leftJoinAndSelect('hash.subcategory', 'subcategory')
      .getMany();

    return hashes;
  }
}
