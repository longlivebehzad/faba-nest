import { IsNotEmpty, IsString } from 'class-validator';

export class VerifyHashDto {
  @IsNotEmpty()
  @IsString()
  hash: string;
}
