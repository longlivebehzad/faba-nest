import { Subcategory } from './../subcategory/subcategory.entity';
import { SubCategoryRepository } from './../subcategory/subcategory.repository';
import { CategoryRepository } from './../category/category.repository';
import { SubcategoryService } from './../subcategory/subcategory.service';
import { CategoryService } from './../category/category.service';
import { Category } from './../category/category.entity';
import { CreateHashDto } from './dto/create-hash.dto';
import { Hash } from './hash.entity';
import { User } from './../auth/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HashRepository } from './hash.repository';

@Injectable()
export class HashesService {
  constructor(
    @InjectRepository(HashRepository)
    private hashRepository: HashRepository,
    @InjectRepository(CategoryRepository)
    private categoryRepository: CategoryRepository,
    @InjectRepository(SubCategoryRepository)
    private subCategoryRepository: SubCategoryRepository,
  ) {}

  async createHash(user: User, createHashDto: CreateHashDto) {
    const { category: categoryId, subCategory: subCategoryId } = createHashDto;
    try {
      const category: Category = await this.categoryRepository.findOne({
        id: categoryId,
      });
      const subCategory: Subcategory = await this.subCategoryRepository.findOne(
        {
          id: subCategoryId,
        },
      );

      // Create hash
      const storedHash: Hash = await this.hashRepository.createHash(
        user,
        createHashDto,
        category,
        subCategory,
      );

      return storedHash;
    } catch (error) {}
  }

  async getUserCategory(user: User) {
    return await this.hashRepository.getUserCategory(user);
  }
}
