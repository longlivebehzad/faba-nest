import { createCategoryDto } from './dto/create-category.dto';
import { User } from 'src/auth/user.entity';
import { Category } from './category.entity';

import { Repository, EntityRepository } from 'typeorm';
import {
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common';

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {
  async createCategory(
    user: User,
    createCategoryDto: createCategoryDto,
  ): Promise<any> {
    try {
      const category = new Category();
      category.name = createCategoryDto.name;
      category.user = user;
      await category.save();
      return category;
    } catch (error) {
      throw error;
    }
  }

  async getCategory(user: User): Promise<any> {
    try {
      const query = this.createQueryBuilder('category');
      const categories: Category[] = await query
        .where('category.user = :user')
        .setParameters({ user: user.id })
        .getMany();
      return categories;
    } catch (error) {
      throw error;
    }
  }
}
