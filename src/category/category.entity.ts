import { User } from 'src/auth/user.entity';
import { Subcategory } from './../subcategory/subcategory.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToOne,
  JoinColumn,
  BaseEntity,
  OneToMany,
} from 'typeorm';

@Entity()
export class Category extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(
    type => Subcategory,
    subcategory => subcategory.category,
  )
  subCategories: Subcategory[];

  @ManyToOne(
    type => User,
    user => user.categories,
  )
  user: User;
}
