import { createCategoryDto } from './dto/create-category.dto';
import { CategoryService } from './category.service';
import {
  Controller,
  Get,
  UseGuards,
  Post,
  Body,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { GetUser } from 'src/auth/get-user.decorator';
import { User } from './../auth/user.entity';
import { AuthGuard } from '@nestjs/passport';

@Controller('category')
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @Post('')
  @UseGuards(AuthGuard())
  @UsePipes(ValidationPipe)
  async createCategory(
    @GetUser() user: User,
    @Body() createCategoryDto: createCategoryDto,
  ): Promise<any> {
    return await this.categoryService.createCategory(user, createCategoryDto);
  }

  @Get('')
  @UseGuards(AuthGuard())
  async getCategory(@GetUser() user: User): Promise<any> {
    return await this.categoryService.getCategory(user);
  }
}
