import { AuthModule } from './../auth/auth.module';
import { PassportModule } from '@nestjs/passport';
import { CategoryRepository } from './category.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CategoryController } from './category.controller';

@Module({
  imports: [
    PassportModule,
    AuthModule,
    TypeOrmModule.forFeature([CategoryRepository]),
  ],
  providers: [CategoryService],
  controllers: [CategoryController],
})
export class CategoryModule {}
