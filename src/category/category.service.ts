import { InjectRepository } from '@nestjs/typeorm';
import { CategoryRepository } from './category.repository';
import { createCategoryDto } from './dto/create-category.dto';
import { User } from 'src/auth/user.entity';
import {
  Injectable,
  Body,
  UsePipes,
  ValidationPipe,
  InternalServerErrorException,
} from '@nestjs/common';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(CategoryRepository)
    private CategoryRepository: CategoryRepository,
  ) {}

  async createCategory(
    user: User,
    createCategoryDto: createCategoryDto,
  ): Promise<any> {
    try {
      return await this.CategoryRepository.createCategory(
        user,
        createCategoryDto,
      );
    } catch (error) {
      throw new InternalServerErrorException('Server Error');
    }
  }

  async getCategory(user: User) {
    try {
      return await this.CategoryRepository.getCategory(user);
    } catch (error) {
      throw new InternalServerErrorException('Server Error');
    }
  }
}
