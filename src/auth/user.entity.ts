import { Category } from './../category/category.entity';
import { Hash } from './../hash/hash.entity';
import {
  BaseEntity,
  Entity,
  ObjectID,
  ObjectIdColumn,
  Column,
  Index,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import * as bcrypt from 'bcrypt';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  @Index({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  salt: string;

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }

  @OneToMany(
    type => Hash,
    hash => hash.user,
  )
  hashes: Hash[];

  @OneToMany(
    type => Category,
    category => category.user,
  )
  categories: Category[];
}
