import {
  IsString,
  MinLength,
  MaxLength,
  IsEmail,
  Matches,
} from 'class-validator';

import { Transform } from 'class-transformer';

export class AuthCredentialsDto {
  @IsString()
  @IsEmail()
  @Transform(email => email.toLowerCase())
  email: string;

  @IsString()
  @MinLength(6)
  @MaxLength(20)
  password: string;
}
