import { User } from 'src/auth/user.entity';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  UseGuards,
  Get,
  Put,
  UsePipes,
} from '@nestjs/common';
import { RegisterDto } from './dto/register.dto';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from './get-user.decorator';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signup')
  @UsePipes(new ValidationPipe({ transform: true }))
  async signUp(
    @Body() registerDto: RegisterDto,
  ): Promise<{ accessToken: string }> {
    return this.authService.signUp(registerDto);
  }

  @Post('/signin')
  @UsePipes(new ValidationPipe({ transform: true }))
  async signIn(
    @Body() authCredentialsDto: AuthCredentialsDto,
  ): Promise<{ accessToken: string }> {
    return await this.authService.signIn(authCredentialsDto);
  }

  @Get('/user-info')
  @UseGuards(AuthGuard())
  userInfo(@GetUser() user: User) {
    return {
      name: user.name,
      email: user.email,
    };
  }
  @Put()
  @UsePipes(new ValidationPipe({ transform: true }))
  @UseGuards(AuthGuard())
  async updateUser(
    @Body() updateUserDto: UpdateUserDto,
    @GetUser() user: User,
  ) {
    return await this.authService.updateUser(updateUserDto, user);
  }
}
