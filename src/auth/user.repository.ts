import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { Repository, EntityRepository, getMongoManager } from 'typeorm';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';
import {
  ConflictException,
  InternalServerErrorException,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { RegisterDto } from './dto/register.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  constructor() {
    super();
  }
  async signUp(registerDto: RegisterDto): Promise<User> {
    const { password, email, name } = registerDto;

    const salt = await bcrypt.genSalt();

    const user = new User();
    user.name = name;
    user.email = email;
    user.salt = salt;
    user.password = await this.hashPassword(password, user.salt);
    const manager = getMongoManager();
    try {
      await manager.save(user);
      return user;
    } catch (error) {
      console.log(error);

      if (error.code === 11000)
        throw new ConflictException('ایمیل قبلا ثبت نام شده است');
      throw new InternalServerErrorException();
    }
  }

  async findByEmail(email: string) {
    const query = this.createQueryBuilder('user');

    const user = await query
      .where('user.email = :email')
      .setParameters({ email })
      .getOne();

    return user || null;
  }

  async updateUser(updateUserDto: UpdateUserDto, user: User) {
    const { email, name, password } = updateUserDto;
    const userIsExist = await this.findByEmail(user.email);
    if (!userIsExist) {
      throw new NotFoundException('کاربر پیدا نشد.');
    }

    const passwornIsValid = await user.validatePassword(password);
    if (!passwornIsValid) {
      throw new BadRequestException('پسورد صحیح نمی باشد');
    }

    user.email = email;
    user.name = name;
    user.password = await this.hashPassword(password, user.salt);

    try {
      await user.save();
      return user;
    } catch (error) {
      return error;
    }
  }

  async validateUserPassword(
    authCredentialsDto: AuthCredentialsDto,
  ): Promise<User> {
    const { email, password } = authCredentialsDto;

    const user = await this.findOne({
      email,
    });
    if (user && (await user.validatePassword(password))) {
      return user;
    }
    return null;
  }

  private async hashPassword(password: string, salet: string): Promise<string> {
    return await bcrypt.hash(password, salet);
  }
}
