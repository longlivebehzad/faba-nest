import { JwtPayload } from './jwt-payload.interface';
import { UserRepository } from './user.repository';
import {
  Injectable,
  UnauthorizedException,
  BadRequestException,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { JwtService } from '@nestjs/jwt';
import { RegisterDto } from './dto/register.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './user.entity';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async signUp(registerDto: RegisterDto): Promise<{ accessToken: string }> {
    const user: User = await this.userRepository.signUp(registerDto);
    const payload: JwtPayload = { email: user.email, name: user.name };
    const accessToken = await this.jwtService.sign(payload);

    return { accessToken };
  }
  async signIn(
    authCredentialsDto: AuthCredentialsDto,
  ): Promise<{ accessToken: string }> {
    const user = await this.userRepository.validateUserPassword(
      authCredentialsDto,
    );
    if (!user) throw new UnauthorizedException('Invalid credentials');
    const payload: JwtPayload = { email: user.email, name: user.name };
    const accessToken = await this.jwtService.sign(payload);

    return { accessToken };
  }

  async updateUser(updateUserDto: UpdateUserDto, user: User) {
    try {
      const updatedUser: User = await this.userRepository.updateUser(
        updateUserDto,
        user,
      );
      const payload: JwtPayload = {
        email: updatedUser.email,
        name: updatedUser.name,
      };
      const accessToken = await this.jwtService.sign(payload);
      return { accessToken };
    } catch (error) {
      if (
        error.response.statusCode === 400 ||
        error.response.statusCode === 404
      ) {
        throw new BadRequestException(error.response.message);
      } else throw new InternalServerErrorException('sa');
    }
  }
}
